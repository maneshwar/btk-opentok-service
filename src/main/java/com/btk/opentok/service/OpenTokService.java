package com.btk.opentok.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.btk.opentok.OpenTokConfig;
import com.btk.opentok.domain.OpenTokConfigDomain;



@Service
public interface OpenTokService {
	
	@Transactional
	public OpenTokConfigDomain prepareConfigObject();
	
	@Transactional
	public OpenTokConfig addOpenConfigObject(OpenTokConfig openTokConfig);
}
