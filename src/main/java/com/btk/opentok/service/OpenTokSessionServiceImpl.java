package com.btk.opentok.service;

import java.util.Date;

import javax.annotation.Resource;

import com.btk.opentok.OpenTokConfig;
import com.btk.opentok.dao.OpenTokSessionDAO;
import com.btk.opentok.dao.OpenTokUserDAO;
import com.btk.opentok.domain.OpenTokSessionDomain;
import com.btk.opentok.domain.UserDomain;
import com.btk.opentok.entity.OpenTokSessionEntity;
import com.btk.opentok.entity.UserEntity;
import com.btk.opentok.enums.SessionType;
import com.btk.opentok.util.EntityUtil;
import com.opentok.MediaMode;
import com.opentok.OpenTok;
import com.opentok.Session;
import com.opentok.SessionProperties;
import com.opentok.exception.OpenTokException;

public class OpenTokSessionServiceImpl implements OpenTokSessionService{
	
	@Resource
	private OpenTokConfig openTokConfig;
	
	@Resource
	private OpenTokUserDAO openTokUserDAO;
	
	@Resource
	private OpenTokSessionDAO openTokSessionDAO;
	
	@Override
	public OpenTokSessionDomain generateRoutedSessionForUser(String userKey, String sessionName, String sessionDescription, Date dateOfSession, String createdForEmail) {
		OpenTokSessionDomain sessionDomain;
		OpenTok openTok = openTokConfig.getOpenTok();
		if(openTok != null){
			Session session = null;
			try {
				session = openTok.createSession(new SessionProperties.Builder()
						.mediaMode(MediaMode.ROUTED).build());
				
				sessionDomain = new OpenTokSessionDomain();
				sessionDomain.setSessionDescription(sessionDescription);
				sessionDomain.setSessionName(sessionName);
				sessionDomain.setSessionString(session.getSessionId());
				sessionDomain.setSessionType(SessionType.ROUTED);
				sessionDomain.setDateOfSession(dateOfSession);
				sessionDomain.setCreatedForEmail(createdForEmail);
				UserEntity userEntity = openTokUserDAO.getUserEntityFromUserKey(userKey);
				/**
				 * Lets Persist the Session
				 */
				 OpenTokSessionEntity sessionEntity = openTokSessionDAO.persistOpenTokSession(sessionDomain,userEntity);
				 /*sessionDomain.setUserDomain(EntityUtil.createUserDomainFromUserEntity(userEntity));*/
				 sessionDomain = EntityUtil.createSessionDomainFromSessionEntity(sessionEntity);
				 return sessionDomain;
			} catch (OpenTokException e) {
				System.err.println(e);
			}
		}
		return null;
	}

	@Override
	public OpenTokSessionDomain generateP2pSession(UserDomain user) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
