package com.btk.opentok.service;

import javax.annotation.Resource;

import com.btk.opentok.OpenTokConfig;
import com.btk.opentok.dao.OpenTokServiceDAO;
import com.btk.opentok.domain.OpenTokConfigDomain;

public class OpenTokServiceImpl implements OpenTokService {

	@Resource
	private OpenTokServiceDAO openTokServiceDAO;

	@Override
	public OpenTokConfigDomain prepareConfigObject() {
		OpenTokConfigDomain domain = new OpenTokConfigDomain();
		OpenTokConfigDomain openTokConfig_ = openTokServiceDAO.getOpenTokServerDetails(domain);
		return openTokConfig_;
	}

	@Override
	public OpenTokConfig addOpenConfigObject(OpenTokConfig openTokConfig) {
		if (openTokConfig != null){
			return openTokServiceDAO.addConfigDetails(openTokConfig);
		}
		return null;
	}

}
