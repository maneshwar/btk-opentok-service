package com.btk.opentok.service;

import javax.annotation.Resource;

import com.btk.opentok.dao.OpenTokUserDAO;
import com.btk.opentok.domain.UserDomain;
import com.btk.opentok.entity.UserEntity;
import com.btk.opentok.util.EntityUtil;

public class OpenTokUserServiceImpl implements OpenTokUserService{

	@Resource
	private OpenTokUserDAO openTokUserDAO;
	
	@Override
	public UserDomain createUserAccount(UserDomain userDomain) {
		UserEntity uEntity = EntityUtil.createUserEntityfromUserDomain(userDomain);
		if(uEntity != null){
			uEntity = openTokUserDAO.createUserEntity(uEntity);
			if(uEntity != null){
				return EntityUtil.createUserDomainFromUserEntity(uEntity);
			}else{
				System.err.println("Error Occured While Processing UserEntity");
			}
		}
		return null;
	}

	@Override
	public UserDomain deleteUserAccount(UserDomain userDomain) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserDomain modifyUserAccount(UserDomain userDomain) {
		// TODO Auto-generated method stub
		return null;
	}

}
