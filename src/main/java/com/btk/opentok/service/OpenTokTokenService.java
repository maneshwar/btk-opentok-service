package com.btk.opentok.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.btk.opentok.domain.OpenTokTokenDomain;

@Service(value="openTokTokenService")
public interface OpenTokTokenService {
	
	@Transactional
	public OpenTokTokenDomain createOpenTokToken(OpenTokTokenDomain tokenDomain);
}
