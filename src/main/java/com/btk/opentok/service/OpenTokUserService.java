package com.btk.opentok.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.btk.opentok.domain.UserDomain;


@Service
public interface OpenTokUserService {
	
	/**
	 * TODO: Adding a security Feature for the application
	 * @description Create User Account for the OpenTok Service
	 * @param userDomain
	 * @return
	 */
	@Transactional
	public UserDomain createUserAccount(UserDomain userDomain);
	
	/**
	 * 
	 * @param userDomain
	 * @return
	 */
	public UserDomain deleteUserAccount(UserDomain userDomain);
	
	/**
	 * 
	 * @param userDomain
	 * @return
	 */
	public UserDomain modifyUserAccount(UserDomain userDomain);
}
