package com.btk.opentok.service;

import javax.annotation.Resource;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.btk.opentok.OpenTokConfig;
import com.btk.opentok.dao.OpenTokSessionDAO;
import com.btk.opentok.dao.OpenTokTokenDAO;
import com.btk.opentok.dao.OpenTokUserDAO;
import com.btk.opentok.domain.OpenTokTokenDomain;
import com.btk.opentok.entity.OpenTokSessionEntity;
import com.btk.opentok.entity.OpenTokTokenEntity;
import com.btk.opentok.entity.UserEntity;
import com.btk.opentok.util.EntityUtil;
import com.opentok.Role;
import com.opentok.TokenOptions;

public class OpenTokTokenServiceImpl implements OpenTokTokenService {
	
	@Resource
	private OpenTokConfig openTokConfig;
	
	@Resource
	private OpenTokTokenDAO openTokTokenDAO;
	
	@Resource
	private OpenTokUserDAO openTokUserDAO;
	
	@Resource
	private OpenTokSessionDAO openTokSessionDAO;
	
	@Override
	public OpenTokTokenDomain createOpenTokToken(OpenTokTokenDomain tokenDomain){		
		try{			
			UserEntity uEntity = openTokUserDAO.getUserEntityFromUserKey(tokenDomain.getCreator().getUserKey());
			OpenTokSessionEntity sessionEntity = openTokSessionDAO.getSessionFromSessionString(tokenDomain.getParentSession().getSessionString());
			String token = null ;
			if(sessionEntity != null && sessionEntity.getSession() != null){
				  String connectionMetadata = "username=maneshwar,userLevel=4";
				TokenOptions tokenOpts = new TokenOptions.Builder()
				          .role(Role.MODERATOR)
				          .expireTime((System.currentTimeMillis() / 1000) + (60 * 60))
				          .data(connectionMetadata)
				          .build();
				token = openTokConfig.getOpenTok().generateToken(sessionEntity.getSession(),tokenOpts);
			}
			tokenDomain.setToken(token);
			OpenTokTokenEntity tokenEntity = EntityUtil.createTokenEntityFromTokenDomain(tokenDomain, uEntity, sessionEntity);
			tokenEntity = openTokTokenDAO.persistToken(tokenEntity);
			tokenDomain = EntityUtil.createTokenDomainFromTokenEntity(tokenEntity);
			return tokenDomain;
		}catch(Exception e){
			System.err.println(e);
		}
		return null;
	}
	
}
