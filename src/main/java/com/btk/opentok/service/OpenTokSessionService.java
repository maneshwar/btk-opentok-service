package com.btk.opentok.service;

import java.util.Date;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.btk.opentok.domain.OpenTokSessionDomain;
import com.btk.opentok.domain.UserDomain;

/**
 * @description Deals with OpenTok Session
 * @author maneshwar
 *
 */
@Service(value="openTokSessionService")
public interface OpenTokSessionService {
	
	/**
	 * 
	 * @param time in seconds
	 * @return
	 */
	@Transactional
	public OpenTokSessionDomain generateRoutedSessionForUser(String userKey, String sessionName, String sessionDescription, Date dateOfSession, String createdForEmail);
	
	/**
	 * 
	 * @param time in seconds
	 * @return
	 */
	public OpenTokSessionDomain generateP2pSession(UserDomain user);
	
}
