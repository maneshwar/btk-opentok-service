package com.btk.opentok.domain;

import com.btk.opentok.enums.Status;

public class RestResponse {
	
	private Status status;
	private Object data;
	private String message;
	
	public Status getStatus() {
		return status;
	}
	public void setStatus(Status status) {
		this.status = status;
	}
	public Object getData() {
		return data;
	}
	public void setData(Object data) {
		this.data = data;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}

}
