package com.btk.opentok.domain;

import java.io.Serializable;

public class OpenTokConfigDomain implements  Serializable, IsDomain{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Integer apiKey;
	private String apiSecret;
	
	public Integer getApiKey() {
		return apiKey;
	}
	public void setApiKey(Integer apiKey) {
		this.apiKey = apiKey;
	}
	public String getApiSecret() {
		return apiSecret;
	}
	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}
	
}
