package com.btk.opentok.domain;

import java.io.Serializable;
import java.util.Date;

import com.btk.opentok.enums.SessionType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties
public class OpenTokSessionDomain implements IsDomain, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Long session_id;
	private UserDomain userDomain;
	private String sessionString;
	private String sessionDescription;
	private String sessionName;
	private SessionType sessionType;
	private Date dateOfSession;
	private String createdForEmail;
	
	
	public OpenTokSessionDomain(){}
	public SessionType getSessionType() {
		return sessionType;
	}
	public void setSessionType(SessionType sessionType) {
		this.sessionType = sessionType;
	}
	
	public OpenTokSessionDomain(Long session_id, UserDomain userDomain, String sessionString, String sessionDescription,
			String sessionName, SessionType sessionType, Date dateOfSession, String createdForEmail) {
		super();
		this.session_id = session_id;
		this.userDomain = userDomain;
		this.sessionString = sessionString;
		this.sessionDescription = sessionDescription;
		this.sessionName = sessionName;
		this.sessionType = sessionType;
		this.dateOfSession = dateOfSession;
		this.createdForEmail = createdForEmail;
	}
	public Long getSession_id() {
		return session_id;
	}
	public void setSession_id(Long session_id) {
		this.session_id = session_id;
	}
	public UserDomain getUserDomain() {
		return userDomain;
	}
	public void setUserDomain(UserDomain userDomain) {
		this.userDomain = userDomain;
	}
	public String getSessionString() {
		return sessionString;
	}
	public void setSessionString(String sessionString) {
		this.sessionString = sessionString;
	}
	public String getSessionDescription() {
		return sessionDescription;
	}
	public void setSessionDescription(String sessionDescription) {
		this.sessionDescription = sessionDescription;
	}
	public String getSessionName() {
		return sessionName;
	}
	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}
	public Date getDateOfSession() {
		return dateOfSession;
	}
	public void setDateOfSession(Date dateOfSession) {
		this.dateOfSession = dateOfSession;
	}
	public String getCreatedForEmail() {
		return createdForEmail;
	}
	public void setCreatedForEmail(String createdForEmail) {
		this.createdForEmail = createdForEmail;
	}
	
}
