package com.btk.opentok.domain;

import java.io.Serializable;

import com.btk.opentok.enums.Role;

public class OpenTokTokenDomain implements IsDomain, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String token;
	
	private OpenTokSessionDomain parentSession;
	
	private UserDomain creator;
	
	private Long duration;
	
	private Role role;
	
	private String tokenData;
	
	private String tokenDescription;
	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public OpenTokSessionDomain getParentSession() {
		return parentSession;
	}

	public void setParentSession(OpenTokSessionDomain parentSession) {
		this.parentSession = parentSession;
	}

	public UserDomain getCreator() {
		return creator;
	}

	public void setCreator(UserDomain creator) {
		this.creator = creator;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public String getTokenData() {
		return tokenData;
	}

	public void setTokenData(String tokenData) {
		this.tokenData = tokenData;
	}

	public String getTokenDescription() {
		return tokenDescription;
	}

	public void setTokenDescription(String tokenDescription) {
		this.tokenDescription = tokenDescription;
	}	
}
