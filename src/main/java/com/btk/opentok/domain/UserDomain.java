package com.btk.opentok.domain;

import java.io.Serializable;
import java.util.Date;


public class UserDomain implements Serializable, IsDomain {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String userName;

	private String userFirstName;

	private String userLastName;

	private String userEmail;

	private String userEmailBackup;

	private Long userContact;

	private String country;
	
	private Date creationData;
	
	private Date updateDate;
	
	private String userKey;
	
	public UserDomain(){}
	
	public UserDomain(String userName, String userFirstName, String userLastName, String userEmail,
			String userEmailBackup, Long userContact, String country, String userKey) {
		super();
		this.userName = userName;
		this.userFirstName = userFirstName;
		this.userLastName = userLastName;
		this.userEmail = userEmail;
		this.userEmailBackup = userEmailBackup;
		this.userContact = userContact;
		this.country = country;
		this.userKey = userKey;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserEmailBackup() {
		return userEmailBackup;
	}

	public void setUserEmailBackup(String userEmailBackup) {
		this.userEmailBackup = userEmailBackup;
	}

	
	public Long getUserContact() {
		return userContact;
	}

	public void setUserContact(Long userContact) {
		this.userContact = userContact;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public Date getCreationData() {
		return creationData;
	}

	public void setCreationData(Date creationData) {
		this.creationData = creationData;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	public String getUserKey() {
		return userKey;
	}

	public void setUserKey(String userKey) {
		this.userKey = userKey;
	}	
}
