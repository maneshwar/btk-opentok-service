package com.btk.opentok.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.btk.opentok.enums.SessionType;

@Entity
@Table(name = "opentok_session")
@NamedQueries({
	@NamedQuery(
	name = "findSessionEntityBySession",
	query = "from OpenTokSessionEntity s where s.session = :sessionString"
	)
})
public class OpenTokSessionEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "session_name")
	private String sessionName;
	@Column(name = "session")
	private String session;		
	@Column(name="isActive")
	private Boolean isActive;	
	@Enumerated(EnumType.STRING)
	@Column(name="session_type")
	private SessionType sessionType;	
	@Column(name="session_description")
	private String sessionDescription;
	@Column(name="session_date")
	private Date dateOfSession;
	
	@Column(name="email")
	private String email;
	@Column(name="email_is_active")
	private Boolean emailIsActive;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getEmailIsActive() {
		return emailIsActive;
	}
	public void setEmailIsActive(Boolean isActive) {
		this.emailIsActive = isActive;
	}	
	
	/**
	 * Can Contribute Back to Discussion
	 */
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "openTokActiveSessionList")
	private List<UserEntity> activeConsumers;
	/**
	 * Can only listen to the conversation
	 */
	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "openTokPassiveSessionList")
	private List<UserEntity> passiveConsumers;

	
	@ManyToOne(fetch = FetchType.LAZY,cascade =  CascadeType.ALL)
	@JoinColumn(name="user_id")
	private UserEntity user;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "session", cascade = CascadeType.ALL)
	private List<OpenTokTokenEntity> tokens;
	
	public String getSessionName() {
		return sessionName;
	}

	public void setSessionName(String sessionName) {
		this.sessionName = sessionName;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}
	
	public List<UserEntity> getActiveConsumers() {
		return activeConsumers;
	}

	public void setActiveConsumers(List<UserEntity> activeConsumers) {
		this.activeConsumers = activeConsumers;
	}

	public List<UserEntity> getPassiveConsumers() {
		return passiveConsumers;
	}

	public void setPassiveConsumers(List<UserEntity> passiveConsumers) {
		this.passiveConsumers = passiveConsumers;
	}

	public UserEntity getUser() {
		return user;
	}

	public void setUser(UserEntity user) {
		this.user = user;
	}

	public List<OpenTokTokenEntity> getTokens() {
		return tokens;
	}

	public void setTokens(List<OpenTokTokenEntity> tokens) {
		this.tokens = tokens;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public SessionType getSessionType() {
		return sessionType;
	}

	public void setSessionType(SessionType sessionType) {
		this.sessionType = sessionType;
	}

	public String getSessionDescription() {
		return sessionDescription;
	}

	public void setSessionDescription(String sessionDescription) {
		this.sessionDescription = sessionDescription;
	}

	public Date getDateOfSession() {
		return dateOfSession;
	}

	public void setDateOfSession(Date dateOfSession) {
		this.dateOfSession = dateOfSession;
	}

	
	
	
}
