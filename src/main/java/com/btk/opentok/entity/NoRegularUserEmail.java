package com.btk.opentok.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class NoRegularUserEmail{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
		
	@Column(name="email", insertable=false, updatable=false)
	private String email;
	@Column(name="is_active", insertable=false, updatable=false)
	private Boolean isActive;
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}	
}
