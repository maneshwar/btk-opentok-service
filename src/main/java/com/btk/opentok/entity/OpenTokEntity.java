package com.btk.opentok.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="opentok_api")
@NamedQueries({
	@NamedQuery(
	name = "getActiveAPICredentials",
	query = "from OpenTokEntity s where s.isActive = :bool"
	)
})
public class OpenTokEntity extends BaseEntity {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Column(name="api_key")
	private Long apiKey;
	@Column(name="secret")
	private String secret;
	@Column (name="isActive")
	private boolean isActive;
	
	public Long getApiKey() {
		return apiKey;
	}
	public void setApiKey(Long apiKey) {
		this.apiKey = apiKey;
	}
	public String getSecret() {
		return secret;
	}
	public void setSecret(String secret) {
		this.secret = secret;
	}
	public boolean isActive() {
		return isActive;
	}
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}	
}
