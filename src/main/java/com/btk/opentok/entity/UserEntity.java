package com.btk.opentok.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * 
 * @author maneshwar
 *
 */
@Entity
@Table(name = "opentok_user")
@NamedQueries({
	@NamedQuery(
	name = "findUserByUserName",
	query = "from UserEntity s where s.userName = :userName"
	)
	,
	@NamedQuery(
			name = "findUserByUserKey",
			query = "from UserEntity s where s.userkey = :userKey"
			)
})
public class UserEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "user_name")
	private String userName;
	@Column(name = "user_first_name")
	private String userFirstName;
	@Column(name = "user_last_name")
	private String userLastName;
	@Column(name = "user_email")
	private String userEmail;
	@Column(name = "user_email_backup")
	private String userEmailBackup;
	@Column(name = "contact")
	private Long userContact;
	@Column(name = "country")
	private String userCountry;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "createBy", cascade = CascadeType.ALL)
	private List<OpenTokTokenEntity> tokens;
	/**
	 * user_name+email encrypted, base64 encoding
	 */
	@Column(name="user_key")
	private String userkey;
	@Column(name="is_active")
	private Boolean isActive;
	
	@OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy="user")
	private List<OpenTokSessionEntity> openTokCreatedSessionList;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "opentok_active_session", catalog = "btk_dev", joinColumns = {
			@JoinColumn(name = "USER_ID", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "SESSION_ID", nullable = false, updatable = false) })
	private List<OpenTokSessionEntity> openTokActiveSessionList;

	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = "opentok_passive_session", catalog = "btk_dev", joinColumns = {
			@JoinColumn(name = "USER_ID", nullable = false) }, inverseJoinColumns = {
					@JoinColumn(name = "SESSION_ID", nullable = false, updatable = false) })
	private List<OpenTokSessionEntity> openTokPassiveSessionList;

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserFirstName() {
		return userFirstName;
	}

	public void setUserFirstName(String userFirstName) {
		this.userFirstName = userFirstName;
	}

	public List<OpenTokSessionEntity> getOpenTokActiveSessionList() {
		return openTokActiveSessionList;
	}

	public void setOpenTokActiveSessionList(List<OpenTokSessionEntity> openTokActiveSessionList) {
		this.openTokActiveSessionList = openTokActiveSessionList;
	}

	public List<OpenTokSessionEntity> getOpenTokPassiveSessionList() {
		return openTokPassiveSessionList;
	}

	public void setOpenTokPassiveSessionList(List<OpenTokSessionEntity> openTokPassiveSessionList) {
		this.openTokPassiveSessionList = openTokPassiveSessionList;
	}

	public String getUserLastName() {
		return userLastName;
	}

	public void setUserLastName(String userLastName) {
		this.userLastName = userLastName;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	public String getUserEmailBackup() {
		return userEmailBackup;
	}

	public void setUserEmailBackup(String userEmailBackup) {
		this.userEmailBackup = userEmailBackup;
	}

	public Long getUserContact() {
		return userContact;
	}

	public void setUserContact(Long userContact) {
		this.userContact = userContact;
	}

	public String getUserCountry() {
		return userCountry;
	}

	public void setUserCountry(String userCountry) {
		this.userCountry = userCountry;
	}

	public List<OpenTokSessionEntity> getOpenTokCreatedSessionList() {
		return openTokCreatedSessionList;
	}

	public void setOpenTokCreatedSessionList(List<OpenTokSessionEntity> openTokCreatedSessionList) {
		this.openTokCreatedSessionList = openTokCreatedSessionList;
	}

	public List<OpenTokTokenEntity> getTokens() {
		return tokens;
	}

	public void setTokens(List<OpenTokTokenEntity> tokens) {
		this.tokens = tokens;
	}


	public String getUserkey() {
		return userkey;
	}

	public void setUserkey(String userkey) {
		this.userkey = userkey;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}	
}
