package com.btk.opentok.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.btk.opentok.enums.Role;

@Entity
@Table(name = "opentok_token")
@NamedQueries({
	@NamedQuery(
	name = "findTokenEntityByToken",
	query = "from OpenTokTokenEntity s where s.token = :tokenString"
	)
})
public class OpenTokTokenEntity extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Column(name="token", length = 2500)
	private String token;
	@Column(name="role")
	@Enumerated(EnumType.STRING)
	private Role role;
	
	@Column(name="is_active")
	private Boolean isActive;
	
	@ManyToOne(fetch = FetchType.LAZY,cascade =  CascadeType.ALL)
	@JoinColumn(name="user_id")
	private UserEntity createBy;
	
	@ManyToOne(fetch = FetchType.LAZY,cascade =  CascadeType.ALL)
	@JoinColumn(name="session_id")
	private OpenTokSessionEntity session;
	
	/**
	 * Duration Is Specified in Seconds
	 */
	@Column(name="duration")
	private Long duration;
	
	@Column(name="data")
	private String tokenData;
	
	public UserEntity getCreateBy() {
		return createBy;
	}

	public void setCreateBy(UserEntity createBy) {
		this.createBy = createBy;
	}

	@Column(name="token_Description")
	private String tokenDescription;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}	

	public OpenTokSessionEntity getSession() {
		return session;
	}

	public void setSession(OpenTokSessionEntity session) {
		this.session = session;
	}

	public String getTokenDescription() {
		return tokenDescription;
	}

	public void setTokenDescription(String tokenDescription) {
		this.tokenDescription = tokenDescription;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public Long getDuration() {
		return duration;
	}

	public void setDuration(Long duration) {
		this.duration = duration;
	}

	public String getTokenData() {
		return tokenData;
	}

	public void setTokenData(String tokenData) {
		this.tokenData = tokenData;
	}	
}
