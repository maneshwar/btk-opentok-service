package com.btk.opentok.util;

import com.btk.opentok.domain.IsDomain;
import com.btk.opentok.domain.RestResponse;
import com.btk.opentok.enums.Status;

public class RestUtil {
	
	public static RestResponse createRestResponseFromDomain(IsDomain domain){
		RestResponse response = new RestResponse();
		response.setData(domain);
		response.setStatus(Status.SUCCESS);				
		return response;
	}
}
