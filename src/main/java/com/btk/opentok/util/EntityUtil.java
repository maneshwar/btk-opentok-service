package com.btk.opentok.util;

import java.util.Date;

import com.btk.opentok.domain.OpenTokSessionDomain;
import com.btk.opentok.domain.OpenTokTokenDomain;
import com.btk.opentok.domain.UserDomain;
import com.btk.opentok.entity.NoRegularUserEmail;
import com.btk.opentok.entity.OpenTokSessionEntity;
import com.btk.opentok.entity.OpenTokTokenEntity;
import com.btk.opentok.entity.UserEntity;

public class EntityUtil {

	public static UserEntity createUserEntityfromUserDomain(UserDomain userDomain) {
		UserEntity uEntity = null;
		if (userDomain != null) {
			uEntity = new UserEntity();
			uEntity.setCreationData(new Date());
			uEntity.setUpdateDate(new Date());
			uEntity.setUserContact(userDomain.getUserContact());
			uEntity.setUserCountry(userDomain.getCountry());
			uEntity.setUserEmail(userDomain.getUserEmail());
			uEntity.setUserEmailBackup(userDomain.getUserEmailBackup());
			uEntity.setUserFirstName(userDomain.getUserFirstName());
			uEntity.setUserLastName(userDomain.getUserLastName());
			uEntity.setUserName(userDomain.getUserName());
			uEntity.setUserkey(userDomain.getUserKey());
		}
		return uEntity;
	}

	public static UserDomain createUserDomainFromUserEntity(UserEntity userEntity) {
		UserDomain uDomain = null;
		if (userEntity != null) {
			uDomain = new UserDomain();
			uDomain.setCreationData(new Date());
			uDomain.setUpdateDate(new Date());
			uDomain.setUserContact(userEntity.getUserContact());
			uDomain.setCountry(userEntity.getUserCountry());
			uDomain.setUserEmail(userEntity.getUserEmail());
			uDomain.setUserEmailBackup(userEntity.getUserEmailBackup());
			uDomain.setUserFirstName(userEntity.getUserFirstName());
			uDomain.setUserLastName(userEntity.getUserLastName());
			uDomain.setUserName(userEntity.getUserName());
			uDomain.setUserKey(userEntity.getUserkey());
		}
		return uDomain;
	}

	public static OpenTokSessionEntity createSessionEntityFromSessionDomain(OpenTokSessionDomain openTokSessionDomain,
			UserEntity userEntity) {
		OpenTokSessionEntity openTokSessionEntity = new OpenTokSessionEntity();
		if (openTokSessionDomain != null) {
			openTokSessionEntity.setSession(openTokSessionDomain.getSessionString());
			openTokSessionEntity.setUser(userEntity);
			openTokSessionEntity.setIsActive(true);
			openTokSessionEntity.setCreationData(new Date());
			openTokSessionEntity.setUpdateDate(new Date());
			openTokSessionEntity.setSessionName(openTokSessionDomain.getSessionName());
			openTokSessionEntity.setSessionType(openTokSessionDomain.getSessionType());
			openTokSessionEntity.setSessionDescription(openTokSessionDomain.getSessionDescription());
			openTokSessionEntity.setDateOfSession(openTokSessionDomain.getDateOfSession());
			openTokSessionEntity.setEmail(openTokSessionDomain.getCreatedForEmail());
			openTokSessionEntity.setEmailIsActive(false);
		}
		return openTokSessionEntity;
	}

	public static OpenTokSessionDomain createSessionDomainFromSessionEntity(OpenTokSessionEntity sessionEntity) {
		if (sessionEntity != null) {
			OpenTokSessionDomain sessionDomain = new OpenTokSessionDomain();
			sessionDomain.setSession_id(sessionEntity.getId());
			sessionDomain.setSessionDescription(sessionEntity.getSessionDescription());
			sessionDomain.setSessionName(sessionEntity.getSessionName());
			sessionDomain.setSessionString(sessionEntity.getSession());
			sessionDomain.setSessionType(sessionEntity.getSessionType());
			sessionDomain.setCreatedForEmail(sessionEntity.getEmail());
			sessionDomain.setDateOfSession(sessionEntity.getDateOfSession());
			sessionDomain.setUserDomain(createUserDomainFromUserEntity(sessionEntity.getUser()));
			return sessionDomain;
		}
		return null;
	}

	public static OpenTokTokenDomain createTokenDomainFromTokenEntity(OpenTokTokenEntity tokenEntity) {
		if(tokenEntity != null){
			OpenTokTokenDomain openTokTokenDomain = new OpenTokTokenDomain();
			openTokTokenDomain.setCreator(createUserDomainFromUserEntity(tokenEntity.getCreateBy()));
			openTokTokenDomain.setDuration(tokenEntity.getDuration());
			openTokTokenDomain.setParentSession(createSessionDomainFromSessionEntity(tokenEntity.getSession()));
			openTokTokenDomain.setRole(tokenEntity.getRole());
			openTokTokenDomain.setToken(tokenEntity.getToken());
			openTokTokenDomain.setTokenData(tokenEntity.getTokenData());
			openTokTokenDomain.setTokenDescription(tokenEntity.getTokenDescription());			
			return openTokTokenDomain;
		}
		return null;
	}

	public static OpenTokTokenEntity createTokenEntityFromTokenDomain(OpenTokTokenDomain tokenDomain,
			UserEntity uEntity, OpenTokSessionEntity sessionEntity) {
		OpenTokTokenEntity tokenEntity = new OpenTokTokenEntity();
		tokenEntity.setCreateBy(uEntity);
		tokenEntity.setCreationData(new Date());
		tokenEntity.setDuration(tokenDomain.getDuration());
		tokenEntity.setIsActive(true);
		tokenEntity.setRole(tokenDomain.getRole());
		tokenEntity.setSession(sessionEntity);
		tokenEntity.setToken(tokenDomain.getToken());
		tokenEntity.setTokenData(tokenDomain.getTokenData());
		tokenEntity.setTokenDescription(tokenDomain.getTokenDescription());
		tokenEntity.setUpdateDate(new Date());
		return tokenEntity;
	}

}
