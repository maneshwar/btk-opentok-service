package com.btk.opentok.enums;

public enum SessionType {	
	ROUTED,
	DIRECT
}
