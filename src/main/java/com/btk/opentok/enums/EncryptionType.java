package com.btk.opentok.enums;

public enum EncryptionType {	
	LOW,
	MEDIUM,
	ADVANCED
}
