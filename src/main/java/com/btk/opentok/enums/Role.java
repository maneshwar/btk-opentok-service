package com.btk.opentok.enums;

public enum Role {
	
	SUBSCRIBER,
	MODERATOR,
	PUBLISHER
}
