package com.btk.opentok.enums;

public enum LocaleType {
	FRENCH,ENGLISH,GERMAN,SPANISH
}
