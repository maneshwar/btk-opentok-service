package com.btk.opentok.controllers;

import java.io.IOException;
import java.util.concurrent.atomic.AtomicLong;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.btk.opentok.OpenTokConfig;
import com.btk.opentok.domain.OpenTokSessionDomain;
import com.btk.opentok.domain.OpenTokTokenDomain;
import com.btk.opentok.domain.UserDomain;
import com.btk.opentok.enums.Role;
import com.btk.opentok.service.OpenTokService;

@RestController
public class OpenTokController {

	@Autowired
	private OpenTokConfig openTokConfig;

	@Resource
	private OpenTokService openTokService;

	@RequestMapping(value = "opentok_api/add/{api_key}/{secret}", method = RequestMethod.GET)
	public Boolean add(@PathVariable(value = "api_key") String api_key, @PathVariable(value = "secret") String secret) {
		try {
			openTokConfig.setApiKey(Integer.parseInt(api_key));
			openTokConfig.setApiSecret(secret);
			openTokService.addOpenConfigObject(openTokConfig);
			return true;
		} catch (Exception e) {
			System.err.println(e);
		}
		return false;
	}

	@RequestMapping(value = "opentok_api/get/{api_key}", method = RequestMethod.GET)
	public OpenTokTokenDomain getApiKey(@PathVariable(value = "api_key") String api_key) {
		try {
			OpenTokTokenDomain openTokTokenDomain = new OpenTokTokenDomain();
			UserDomain domain = new UserDomain();
			domain.setCountry("Germany");
			domain.setUserEmail("maneshwar50@gmail.com");
			domain.setUserEmailBackup("s@yahoo.com");
			domain.setUserFirstName("Maneshwar");
			domain.setUserLastName("Singh");
			domain.setUserName("maneshwar");
			domain.setUserKey("maneshwar");
			openTokTokenDomain.setCreator(domain);
			openTokTokenDomain.setDuration(120L);
			OpenTokSessionDomain sessionDomain = new OpenTokSessionDomain();
			sessionDomain.setSessionString("2_MX40NTM1MTE1Mn5-MTQ0Mzk3NzEyNzc3Nn50Ky85anVKVGd6cEhZSkZZcXUrbHprTzZ-fg");
			openTokTokenDomain.setParentSession(sessionDomain);
			openTokTokenDomain.setRole(Role.MODERATOR);
			openTokTokenDomain.setTokenData("A Test Token");
			openTokTokenDomain.setTokenDescription("test Domain- Description");
			
			return openTokTokenDomain;
			/*UserDomain domain = new UserDomain();
			domain.setCountry("Germany");
			domain.setUserEmail("maneshwar50@gmail.com");
			domain.setUserEmailBackup("s@yahoo.com");
			domain.setUserFirstName("Maneshwar");
			domain.setUserLastName("Singh");
			domain.setUserName("maneshwar");
			domain.setUserKey("maneshwar");*/
			//return openTokTokenDomain;
			//return openTokConfig.getApiKey().toString();
		} catch (Exception e) {
			System.err.println(e);
		}
		return null;
	}

	/*public @ExceptionHandler(IOException.class) @ResponseBody @ResponseStatus(HttpStatus.BAD_REQUEST) public RestResponse IOException(
			IOException ex) {
		RestResponse rr = new RestResponse();
		ErrorDomain error = new ErrorDomain();
		error.setCode("12345");
		error.setMessage("Error Message");
		rr.setData(error);
		rr.setStatus(Status.ERROR);
		rr.setMessage("IO Exception" + ex.getMessage());
		return rr;
	}*/
}
