package com.btk.opentok.controllers;

import javax.annotation.Resource;

import org.springframework.beans.factory.config.CustomEditorConfigurer;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.btk.opentok.OpenTokConfig;
import com.btk.opentok.domain.OpenTokSessionDomain;
import com.btk.opentok.domain.OpenTokTokenCustomDomain;
import com.btk.opentok.domain.OpenTokTokenDomain;
import com.btk.opentok.domain.UserDomain;
import com.btk.opentok.service.OpenTokTokenService;

@RestController
public class OpenTokTokenController {
	
	@Resource
	private OpenTokConfig openTokConfig;
	
	@Resource
	private OpenTokTokenService openTokTokenService;
		
	
	@RequestMapping(value = "opentok_token_api/create/{token}", method = RequestMethod.POST)
	public OpenTokTokenDomain generateToken(@RequestBody OpenTokTokenDomain token){
		if(token != null){
			return openTokTokenService.createOpenTokToken(token);
		}
		return null;
	}
	
	@RequestMapping(value = "opentok_token_api/create_token/{user}/{session}", method = RequestMethod.GET)
	public OpenTokTokenCustomDomain generateTokenCustom(@PathVariable(value="user") String user, @PathVariable(value="session") String session){
		OpenTokTokenCustomDomain customDomain = new OpenTokTokenCustomDomain();
		/*customDomain.setApiKey(openTokConfig.getApiKey().toString());
		customDomain.setSessionId(session);
		customDomain.setToken("T1==cGFydG5lcl9pZD00NTM1MTE1MiZzaWc9YjlmZGZmZDc1MGViYmJkMzQ1NDg4NTA1ZGYxOTliMmNlYThiODExMTpzZXNzaW9uX2lkPTJfTVg0ME5UTTFNVEUxTW41LU1UUTBOREUyTXpnek9USTBOWDVpZDJGeFVXZHJlSEp1WTBoWGEyTk5hME0xZDJ3M2JIQi1mZyZjcmVhdGVfdGltZT0xNDQ0MjE3MjgyJm5vbmNlPS00MTQwMTQ1MDMmcm9sZT1tb2RlcmF0b3ImZXhwaXJlX3RpbWU9MTQ0NDIyMDg4MiZjb25uZWN0aW9uX2RhdGE9dXNlcm5hbWUlM0RtYW5lc2h3YXIlMkN1c2VyTGV2ZWwlM0Q0");
	*/	UserDomain uDomain = new UserDomain();
		uDomain.setUserKey(user);
		OpenTokSessionDomain openTokSessionDomain = new OpenTokSessionDomain();
		openTokSessionDomain.setSessionString(session);
		OpenTokTokenDomain domain = new OpenTokTokenDomain();
		domain.setCreator(uDomain);
		domain.setParentSession(openTokSessionDomain);
		domain =  this.generateToken(domain);
		customDomain.setApiKey(openTokConfig.getApiKey().toString());
		customDomain.setSessionId(session);
		customDomain.setToken(domain.getToken());
		return customDomain;
	}
}
