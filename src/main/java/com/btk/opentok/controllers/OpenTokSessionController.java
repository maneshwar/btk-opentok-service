package com.btk.opentok.controllers;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.btk.opentok.domain.OpenTokSessionDomain;
import com.btk.opentok.domain.RestResponse;
import com.btk.opentok.service.OpenTokSessionService;
import com.btk.opentok.util.RestUtil;

@RestController
public class OpenTokSessionController {

	@Resource
	private OpenTokSessionService openTokSessionService;

	@RequestMapping(value = "opentok_session_api/create/{user_key}/{session_name}/{session_description}", method = RequestMethod.GET)
	public OpenTokSessionDomain add(@PathVariable(value = "user_key") String userKey,
			@PathVariable(value = "session_name") String sessionName,
			@PathVariable(value = "session_description") String sessionDescription) {
		try {
			OpenTokSessionDomain sessionDomain = openTokSessionService.generateRoutedSessionForUser(userKey,
					sessionName, sessionDescription, null,null);
			return sessionDomain;
		} catch (Exception e) {
			System.err.println(e);
		}
		return null;
	}

	@RequestMapping(value = "opentok_session_api/create_session/{session}", method = RequestMethod.POST)
	public OpenTokSessionDomain createUserToService(@RequestBody OpenTokSessionDomain session) throws Exception {
		OpenTokSessionDomain sessionDomain = openTokSessionService.generateRoutedSessionForUser(
				session.getUserDomain().getUserKey(), session.getSessionName(), session.getSessionDescription(),
				session.getDateOfSession(), session.getCreatedForEmail());
		return sessionDomain;
	}
}
