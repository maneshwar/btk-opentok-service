package com.btk.opentok.controllers;

import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.btk.opentok.OpenTokConfig;
import com.btk.opentok.domain.ErrorDomain;
import com.btk.opentok.domain.RestResponse;
import com.btk.opentok.domain.UserDomain;
import com.btk.opentok.enums.Status;
import com.btk.opentok.service.OpenTokUserService;
import com.btk.opentok.util.RestUtil;

@RestController
public class OpenTokUserController {
	
	
	@Resource
	private OpenTokUserService openTokUserService;
	
	@RequestMapping(value = " /create/{user}", method=RequestMethod.POST)
	public RestResponse createUserToService(@RequestBody UserDomain user) throws Exception{		
		UserDomain userDomain = openTokUserService.createUserAccount(user);
		return RestUtil.createRestResponseFromDomain(userDomain);
	}
	
		
	@ExceptionHandler(Exception.class)
	@ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public RestResponse handleIOException(IOException ex) {
	    RestResponse rr = new RestResponse();
		ErrorDomain error = new ErrorDomain();
	    error.setCode("500");
	    error.setMessage("Error Message");
	    rr.setData(error);rr.setStatus(Status.ERROR);rr.setMessage("Message: There is No Response From Service, Check Server Logs");
		return rr;
	  }
}
