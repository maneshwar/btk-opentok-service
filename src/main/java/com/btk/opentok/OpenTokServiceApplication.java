package com.btk.opentok;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

@Configuration
@ComponentScan("com.btk.opentok")
@EnableAutoConfiguration(exclude=org.springframework.boot.autoconfigure.orm.jpa.HibernateJpaAutoConfiguration.class)
@SpringBootApplication
@ImportResource({"classpath*:spring-config/applicationContext.xml","classpath*:spring-config/daoContext.xml"})
public class OpenTokServiceApplication {

	public static void main(String[] args) {

		System.out.println("Initiating OpenTok Service!");
		try {
			SpringApplication.run(OpenTokServiceApplication.class, args);
		} catch (Exception e) {
			System.out.println("Error Occured During Initialization");
			e.printStackTrace();
		}
		System.out.println("Initiation Completed");
		
	}

}
