package com.btk.opentok.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.btk.opentok.entity.OpenTokTokenEntity;

public class OpenTokTokenDAOImpl implements OpenTokTokenDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public OpenTokTokenEntity persistToken(OpenTokTokenEntity openTokTokenEntity) {
		try{
			entityManager.persist(openTokTokenEntity);
			Query query =  entityManager.createNamedQuery("findTokenEntityByToken").setParameter("tokenString", openTokTokenEntity.getToken());
			if(query != null && query.getResultList().size()>0){
				return (OpenTokTokenEntity) query.getResultList().get(0);
			}
		}catch(Exception e){
			System.err.println(e);
		}
		return null;
	}
	
	
	

}
