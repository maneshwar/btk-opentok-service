package com.btk.opentok.dao;

import org.springframework.stereotype.Repository;

import com.btk.opentok.entity.OpenTokTokenEntity;

@Repository(value = "openTokTokenDAO")
public interface OpenTokTokenDAO {
	
	public OpenTokTokenEntity persistToken(OpenTokTokenEntity openTokTokenEntity);
	
	
	
}
