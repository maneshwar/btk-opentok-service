package com.btk.opentok.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.util.StringUtils;

import com.btk.opentok.entity.UserEntity;

public class OpenTokUserDAOImpl implements OpenTokUserDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public UserEntity createUserEntity(UserEntity userEntity) {
		if (userEntity != null) {
			try {
				entityManager.persist(userEntity);
				Query query = entityManager.createNamedQuery("findUserByUserName").setParameter("userName", userEntity.getUserName());
				if(query != null && query.getResultList().size() == 1){
					return (UserEntity)query.getResultList().get(0);
				}			
			} catch (Exception e) {
				System.err.println("Error Occur While Persisting UserEntity>> "+e);
			}
		}
		return userEntity;
	}

	@Override
	public UserEntity getUserEntityFromUserKey(String userKey) {
		if(!StringUtils.isEmpty(userKey)){
			try{
			Query query = entityManager.createNamedQuery("findUserByUserKey").setParameter("userKey", userKey);
			if(query != null && query.getResultList().size() == 1){
				return (UserEntity)query.getResultList().get(0);
			}
			}catch(Exception e){
				System.err.println("Error Occur While Searching UserEntity From UserKey>> "+e);
			}
		}
		return null;
	}
}
