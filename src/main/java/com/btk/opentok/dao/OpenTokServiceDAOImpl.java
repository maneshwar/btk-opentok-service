package com.btk.opentok.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.btk.opentok.OpenTokConfig;
import com.btk.opentok.domain.OpenTokConfigDomain;
import com.btk.opentok.entity.OpenTokEntity;

public class OpenTokServiceDAOImpl implements OpenTokServiceDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public OpenTokConfigDomain getOpenTokServerDetails(OpenTokConfigDomain openTokConfig) {
		try {
			Query query = entityManager.createNamedQuery("getActiveAPICredentials").setParameter("bool", true);
			OpenTokEntity openTokEntity = null;
			if (query != null && query.getResultList().size() > 0)
				openTokEntity = (OpenTokEntity) query.getResultList().get(0);
				openTokConfig.setApiKey(openTokEntity.getApiKey().intValue());
				openTokConfig.setApiSecret(openTokEntity.getSecret());
			return openTokConfig;
		} catch (Exception e) {
			System.err.println(e);
		}
		return null;
	}

	@Override
	public OpenTokConfig addConfigDetails(OpenTokConfig opConfig) {
		if (opConfig != null) {
			try {
				Query query = entityManager.createNamedQuery("getActiveAPICredentials").setParameter("bool", true);
				/**
				 * TODO: Code Modification to make the conditions more neat, not
				 * a bottleneck though :)
				 */
				if (query != null) {
					if (query.getResultList() != null && query.getResultList().size() == 1) {
						OpenTokEntity openTokEntity = (OpenTokEntity) query.getResultList().get(0);
						openTokEntity.setActive(false);
						entityManager.merge(openTokEntity);
					}
					OpenTokEntity entity = new OpenTokEntity();
					entity.setApiKey(opConfig.getApiKey().longValue());
					entity.setSecret(opConfig.getApiSecret());
					entity.setActive(true);
					entityManager.persist(entity);
					return opConfig;
				}
			} catch (Exception e) {
				System.err.println(e);
			}
		}
		return null;
	}
}
