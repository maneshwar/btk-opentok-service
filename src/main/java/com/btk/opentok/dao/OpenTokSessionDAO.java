package com.btk.opentok.dao;

import org.springframework.stereotype.Repository;

import com.btk.opentok.domain.OpenTokSessionDomain;
import com.btk.opentok.entity.OpenTokSessionEntity;
import com.btk.opentok.entity.UserEntity;

@Repository(value="openTokSessionDAO")
public interface OpenTokSessionDAO {
	
	public OpenTokSessionEntity persistOpenTokSession(OpenTokSessionDomain openTokSessionDomain, UserEntity userEntity);
	
	public OpenTokSessionEntity getSessionFromSessionString(String sessionString);

}
