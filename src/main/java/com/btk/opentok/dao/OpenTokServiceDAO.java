package com.btk.opentok.dao;

import org.springframework.stereotype.Repository;

import com.btk.opentok.OpenTokConfig;
import com.btk.opentok.domain.OpenTokConfigDomain;
import com.btk.opentok.entity.OpenTokEntity;

@Repository
public interface OpenTokServiceDAO {
	
	public OpenTokConfigDomain getOpenTokServerDetails(OpenTokConfigDomain openTokConfig);
	
	public OpenTokConfig addConfigDetails(OpenTokConfig opConfig);
}
