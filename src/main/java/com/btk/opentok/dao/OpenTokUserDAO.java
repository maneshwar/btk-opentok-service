package com.btk.opentok.dao;

import org.springframework.stereotype.Repository;

import com.btk.opentok.entity.UserEntity;

@Repository
public interface OpenTokUserDAO {
	
	public UserEntity createUserEntity(UserEntity userEntity);
	
	public UserEntity getUserEntityFromUserKey(String userString);
}
