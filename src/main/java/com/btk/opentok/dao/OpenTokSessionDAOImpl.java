package com.btk.opentok.dao;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.btk.opentok.domain.OpenTokSessionDomain;
import com.btk.opentok.entity.OpenTokSessionEntity;
import com.btk.opentok.entity.UserEntity;
import com.btk.opentok.util.EntityUtil;

public class OpenTokSessionDAOImpl implements OpenTokSessionDAO {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public OpenTokSessionEntity persistOpenTokSession(OpenTokSessionDomain openTokSessionDomain,
			UserEntity userEntity) {

		OpenTokSessionEntity sessionEntity = EntityUtil.createSessionEntityFromSessionDomain(openTokSessionDomain,
				userEntity);
		try {
			entityManager.persist(sessionEntity);

			entityManager.flush();
			Query query = entityManager.createNamedQuery("findSessionEntityBySession").setParameter("sessionString",
					openTokSessionDomain.getSessionString());
			if (query != null && query.getResultList() != null && query.getResultList().size() > 0)
				return (OpenTokSessionEntity) query.getResultList().get(0);

		} catch (Exception e) {
			System.err.println(e);
		}
		return null;
	}

	@Override
	public OpenTokSessionEntity getSessionFromSessionString(String sessionString) {
		try {
			Query query = entityManager.createNamedQuery("findSessionEntityBySession").setParameter("sessionString",
					sessionString);
			if (query != null && query.getResultList() != null && query.getResultList().size() > 0)
				return (OpenTokSessionEntity) query.getResultList().get(0);
		} catch (Exception e) {
			System.err.println(e);
		}
		return null;
	}

}
