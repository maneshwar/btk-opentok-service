package com.btk.opentok;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import com.btk.opentok.domain.OpenTokConfigDomain;
import com.btk.opentok.service.OpenTokService;
import com.opentok.OpenTok;

public class OpenTokConfig {
	
	@Resource
	private OpenTokService openTokService;
	
	private Integer apiKey;
	private String apiSecret;
	
	private OpenTok openTok;
	
	private OpenTokConfig(){}
	
	@PostConstruct
	private void setUpOpenTok(){
		OpenTokConfigDomain config = openTokService.prepareConfigObject();
		if(config != null){
			this.apiKey = config.getApiKey();
			this.apiSecret = config.getApiSecret();
			openTok = new OpenTok(this.apiKey, this.apiSecret);
		}
	}
	
	public Integer getApiKey() {
		return apiKey;
	}

	public void setApiKey(Integer apiKey) {
		this.apiKey = apiKey;
	}

	public String getApiSecret() {
		return apiSecret;
	}

	public void setApiSecret(String apiSecret) {
		this.apiSecret = apiSecret;
	}

	public OpenTok getOpenTok() {
		return openTok;
	}

	public void setOpenTok(OpenTok openTok) {
		this.openTok = openTok;
	}	
}
