-- Database: opentok_dev

-- DROP DATABASE opentok_dev;

CREATE DATABASE opentok_dev
  WITH OWNER = postgres
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'English_United States.1252'
       LC_CTYPE = 'English_United States.1252'
       CONNECTION LIMIT = -1;

